#Introduction

Hello! This is documentation for Amares.

It is clean, onepage, HTML5, CSS3 multipurpose template. Designed for agency, personal portfolio, app landing page and similar.

Documentation is responsive, you can take it with you on your device and read on the move. For anything beyond the scope of documentation, visit support forum or contact us via form on our profile.

#Overview

If you want to know how does it works - read this chapter. Otherwise go to the chapter "Getting started".

##Bootstrap

The current version of this template is Bootstrap 3. Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.

Bootstrap makes front-end web development faster and easier. It's made for folks of all skill levels, devices of all shapes, and projects of all sizes.
- HTML5
- CSS3
- BootstrapJS

###CSS3

Cascading Style Sheets (CSS) is a style sheet language used for describing the look and formatting of a document written in a markup language.

CSS is designed primarily to enable the separation of document content from document presentation, including elements such as the layout, colors, and fonts. This separation can improve content accessibility, provide more flexibility and control in the specification of presentation characteristics, enable multiple HTML pages to share formatting by specifying the relevant CSS in a separate .css file, and reduce complexity and repetition in the structural content, such as semantically insignificant tables that were widely used to format pages before consistent CSS rendering was available in all major browsers.

Bootstrap makes use of certain HTML elements and CSS properties that require the use of the HTML5 doctype. Include it at the beginning of all your projects. It has a grid system.

####Grid system

Bootstrap includes a responsive, mobile first fluid grid system that appropriately scales up to 12 columns as the device or viewport size increases. It includes predefined classes for easy layout options, as well as powerful mixins for generating more semantic layouts.

Grid systems are used for creating page layouts through a series of rows and columns that house your content. Here's how the Bootstrap grid system works:

- Rows must be placed within a *.container* (fixed-width) or *.container-fluid* (full-width) for proper alignment and padding.
- Use rows to create horizontal groups of columns.
- Content should be placed within columns, and only columns may be immediate children of rows.
- Predefined grid classes like *.row* and *.col-xs-4* are available for quickly making grid layouts. Less mixins can also be used for more semantic layouts.
- Columns create gutters (gaps between column content) via *padding*. That padding is offset in rows for the first and last column via negative margin on .rows.
- The negative margin is why the examples below are outdented. It's so that content within grid columns is lined up with non-grid content.
- Grid columns are created by specifying the number of twelve available columns you wish to span. For example, three equal columns would use three *.col-xs-4*.
- If more than 12 columns are placed within a single row, each group of extra columns will, as one unit, wrap onto a new line.
- Grid classes apply to devices with screen widths greater than or equal to the breakpoint sizes, and override grid classes targeted at smaller devices. Therefore, e.g. applying any *.col-md-* class to an element will not only affect its styling on medium devices but also on large devices if a *.col-lg-* class is not present.

Quick help, working with Bootstrap 12 column grid:
- *col-xs-6* is half site width on small size devices (phones, less than 768px).
- *col-xs-12* would be full width on that device.
- *col-sm-4* is 1/3 site width on medium size devices (tablets, 768px and up).
- *col-sm-3* would be 1/4 site width on that device.

There are *col-lg, col-md, col-sm* and *col-xs*.

<a class='button' href='http://getbootstrap.com/css/#grid-example-basic' target='_blank'>View resource</a>


###Bootstrap.js

Plugins can be included individually (using Bootstrap's individual *.js* files), or all at once (using *bootstrap.js* or the minified *bootstrap.min.js*).

> Using the compiled JavaScript:

> <strong>Both *bootstrap.js* and *bootstrap.min.js* contain all plugins in a single file. Include only one.</strong>

You can use all Bootstrap plugins purely through the markup API without writing a single line of JavaScript. This is Bootstrap's first-class API and should be your first consideration when using a plugin.

<a class='button' href='http://getbootstrap.com/javascript' target='_blank'>View resource</a>

##Jquery

Query is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has changed the way that millions of people write JavaScript.

<a class='button' href='http://jquery.com' target='_blank'>View resource</a>

###Retina

Retina.js is a jquery plugin and an open source script that makes it easy to serve 
high-resolution images to devices with retina displays.

When your users load a page, retina.js checks each image on the page to see if there is a high-resolution version of that image on your server. If a high-resolution variant exists, the script will swap in that image in-place.

>For example, if you have an image on your page that looks like this:
>*img src="/images/my_image.png"*.

>The script will check your server to see if an alternative image exists at this path:
>*"/images/my_image@2x.png"*

<a class='button' href='http://imulus.github.io/retinajs/' target='_blank'>View resource</a>

###ScrollTo.js

This plugin is intended to fast scroll and has a large number of possibilities such as rewind to a specific element in a specific position (specified in *px* or *%*) and not only at what it all with advanced settings (the animation, scrolling axis, etc.). Of course the transition to a certain element can be done with the help of anchors, but in this case it does not turn a smooth transition.

<a class='button' href='https://github.com/flesler/jquery.scrollTo' target='_blank'>View on Github</a>

###NiceScroll.js

Nicescroll plugin allows you to make standard scroll bars style looks similar to that used in ios. Supports change both vertical and horizontal scroll bar.

>Initial for html and modal:
>
<pre><code>
$('html, .modal').niceScroll({
>	//your parameters
>});
</pre></code>

<a class='button' href='https://github.com/inuyaksa/jquery.nicescroll' target='_blank'>View on Github</a>

###Smoothscroll.js

Plugin for smooth scrolling by using the mouse wheel and keyboard in Google Chrome.

<a class='button' href='https://gist.github.com/galambalazs/6477177/' target='_blank'>View on Github</a>

###Backstretch.js

This plugin allows you to add dynamically resize, slide show with support for background image to any page or element. The image will be stretched to fit the page / item and automatically resizes as resizing windows / elements.

<a class='button' href='http://srobbin.com/jquery-plugins/backstretch/' target='_blank'>View resource</a>

###Modernizr.js

When you load the browser and check the possibility of a given class. For example, if a class does not support 'borderradius', then sets modernizr.js class 'no-borderradius'


>Examples:
><p>1. Define the style for the new browsers and define a style for the old:</p>
<pre><code>
.button {
	background: #000;
    opacity: 0.75;
}
.no-opacity .button {
    background: #444;
 }</code></pre>
><p>2. At first, we need to set the style for older browsers, and then for browsers with support for this style:
<pre><code>
.button {
    background: #444;
}
.opacity .button {
    background: #000;
    opacity: 0.75;
}
</code></pre>

<a class='button' href='http://modernizr.com/docs/' target='_blank'>View resource</a>

#Getting started

If you want to set up this template yourself, welcome to the chapter "Getting started

##index.html

For editing files on local server you will need one of the text editor softwares.

For example if you have Windows OS you need Notepad++, or if you have Linux... well, you know what you need.

###Customize build

Now I want you to watch the meta-tags. If you want to customize you site you need change current values for your own. So let's get started!

####Viewport

This meta-tag is responsible for displaying your website on various multimedia devices, for example your smartphone.

><pre><code>&lt;meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"&gt</code></pre>

####Description, keywords

Two next meta-tags are using for description of your web site. It's need it for the search engines (such as Google, Yandex, Rambler), as well as for SEO optimization. Try to specify in these tags words most aptly displays the contents of, or the idea of your website.

>For example: 
><pre><code>&lt;meta name="description" content="Responsive, Mobile First, Retina, Bootstrap 3, One Page, Multi Page, Multi-Purpose, Agency, Clean, Creative, Minimal""&gt</code></pre>

><pre><code>&lt;meta name="keywords" content="Responsive, Mobile First, Retina, Bootstrap 3, One Page, Agency, Clean, Creative, Minimal, Amares, Apares.pro, javascript, node, nodejs, jquery"&gt</code></pre>

####Author and content

Just put there author's name and content.

####http-equiv

Browsers convert the value of the attribute http-equiv, specified with the content, format HTTP response header, and treated them as if they came directly from the server. Not necessary to change.

####yandex-verification

You can request a Yandex.Webmaster check the current status of the confirmation of the right to manage the site. The request must specify the chosen route of site verification. For the test results using resource. 

<a class='button' href='https://tech.yandex.ru/webmaster/doc/dg/reference/hosts-verify-docpage/' target='_blank'>View resource</a>

><pre><code>&lt;meta name='yandex-verification' content='Put here your own uin'/&gt</code></pre>

####modernizr.js

Loading jquery modernizr.js plug-in.

####bootstrap.min.css

Loading minified Bootstrap css library.

####style.css

Entering main css file for your site.

####font-awesome.css

Loading some cool icons like this:

<i class="fa fa-heart"></i> <i class="fa fa-heart fa-2x"></i> <i class="fa fa-heart fa-3x"></i> <i class="fa fa-heart fa-4x"></i> <i class="fa fa-heart fa-5x"></i> <i class="fa fa-heart-o fa-5x"></i> <i class="fa fa-heart-o fa-4x"></i> <i class="fa fa-heart-o fa-3x"></i> <i class="fa fa-heart-o fa-2x"></i> <i class="fa fa-heart-o"></i>

>To use this icons you need to put this code into your html:

><pre><code>&lt;i class="fa fa-heart"&gt</code></pre>

<a class='button' href='http://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>More awesomes</a>

####Font

Loading fonts from Google CDN. You can change parameters of this font or you can change the font.

><pre><code>&lt;link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet"&gt</code></pre>

<a class='button' href='http://www.google.com/fonts/' target='_blank'>More fonts</a>

####Thumbnail and favicon

Go to the folder */public* and put your own thumbnail and favicon.

####Sections

OK! Now i want to tell you something about organization of main page. As you can watch at the html-code there are few section. Each section is a part of our page. You can add, edit, change places or remove it.

Let's look more. Do you see this code for example at 114 line? This is start of our section. It uses *bootstrap grid* and *style.css* style. Play with it!

##custom.js

This is main sript of your site.

Functions:

1. Fix for Internet Explorer 10 in Windows 8 and Windows Phone 8
- Animated elements
- Custom Scrollbar
- Carousels
- Intro section height
- Contact form
- SVG FTW!
- Google map
- Collapse menu on click
- Responsive videos
- Smooth Scroll on Anchors
- Preloader

####Viewport

As I said viewport is responsible for displaying your website on various multimedia devices. THis part of our script is respond for displaying at IEMobile v10. But that screws up some Windows Phone 8 phones, overriding the meta viewport tag and rendering as far too large on small screens.

><pre><code>
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	var msViewportStyle = document.createElement("style")
	msViewportStyle.appendChild(
		document.createTextNode(
			"@-ms-viewport{width:auto!important}"
		)
	)
	document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
}
</code></pre>

####Animated elements

After all images are loaded and if it's no-touch device, run script. If logo over image is not visible due to light/dark color, change logo image on scroll append css to logo image div, change width and height to match your logo size.

><pre><code>$('.navbar-brand div').css({'width':'123px', 'height':'20px'});
</code></pre>

####Custom scrollbar

This is parametres of a custom scrollbar. If you want to change the scroll - change parametres.

><pre><code>
$('html, .modal').niceScroll({
	scrollspeed: 60,
	mousescrollstep: 35,
	cursorwidth: 10,
	cursorborder: 0,
	cursorcolor: '#e3e4e5',
	cursorborderradius: '3px',
	autohidemode: false,
	horizrailenabled: false,
	smoothscroll: false,
});
</code></pre>

####Carousels

You can edit interval of text slide cycle in custom.js file under carousels - in milliseconds.

><pre><code>
$('#carousel_fade_intro').carousel({
    interval: 2500,
    pause: "false"
}) 
</code><pre>

####Intro section height

Make intro section height of viewport. Min height is set to *445px* in *style.css*.

####Contact form

Augments your form labels behave like placeholders, shrinking and moving out of the way on active fields. It uses semantic labels, rather than actual placeholders.

####SVG FTW!

No! Not WTF... This code we need for animat SVG (Scalable Vector Graphics) icons.
Look for example and more icons here:

<a class='button' href='http://tympanus.net/codrops/2013/11/27/svg-icons-ftw/' target='_blank'>View resource</a>

####Gallery lightbox

Lightbox used to display images and other web content similar to a modal dialogs where the image is shown up center filling most of the screen, and the rest of the window is dimmed out.

####Google map

Google Maps is a Web-based service that provides detailed information about geographical regions and sites around the world. In addition to conventional road maps, Google Maps offers aerial and satellite views of many places. In some cities, Google Maps offers street views comprising photographs taken from vehicles.

You can changes the icon of place. It is in *img/map-pin.png*. And don't forget to change parametres *latitude* and *longitude* for your own and enjoy!

####Collapse

Collapse menu on click on mobile and tablet devices.

####Responsive videos

When you embed a video player in a page, you will notice that it does not resize with the browser window. This occurs because, by default, the video player is given a fixed width and height, preventing it from responding to changes in the browser. One aspect of responsive web design is ensuring that elements within your page adapt gracefully to different screen sizes.

####Smooth Scroll on Anchors

Here’s a neat little jQuery trick to fancy up your internal anchor links, by making them scroll smoothly to their target as opposed to jumping instantly.

1. Set up your link as you normally would
<pre><code>&lt;href=”#comments”&gt(where comments is the id of your target)</code></pre>
2. Add a *class=”scroll”* attribute to the link element, so it now looks something like this:
<pre><code>&lt;a href="#comments" class="scroll"&gtScroll to comments&lt;/a&gt</code></pre>
3. Finally add the following jQuery code wherever is most appropriate.

><pre><code>
$('.scroll-btn, #more, .hidden-xs, .nav, .navbar-header, #footer li').localScroll({
	target: 'body',
	queue: true,
	duration: 1000,
	hash: false,
	offset: -60,
	easing: 'easeInOutExpo'
});
</code></pre>

####Preloader

It hides annoying loading of images and keeps user out from scrolling down when scripts are not loaded yet. You can edit background color in style.css under part *12) Preloader*

#Support

You can contact us via e-mail: denis.benyaminov@gmail.com

<a class='button' href='' target='_blank'>Contact us</a>

#####Custom requests and freelance work

If you would like to see some new elements, have a suggestion or in need of customization feel free to contact us.

#About us

Amares.pro build by Websailors.

- **[vk.com](http://vk.com/amares_pro)**
- **[wsailors.com](http://wsailors.com/contactus)**